


let  distance_matrix =require('google-distance-matrix');
let functions = require('firebase-functions');
let admin=require('firebase-admin')
admin.initializeApp({
    credential:admin.credential.applicationDefault(),
    databaseURL:"https://shivaya-6c9f9.firebaseio.com/"
})

distance_matrix.key('AIzaSyBBX6pCmyvDIFmrD3FAh7WzDpls0kfOTZg')



var Waste = /** @class */ (function () {
    
       function Waste( SourceLat, SourceLon, SourceId, SourceStatus,SourceType,Distance,Duration,Paths) {
           this.sourceId = SourceId
           this.sourceLat = SourceLat
           this.sourceLon = SourceLon
           this.sourceStatus = SourceStatus
           this.distance=Distance
           this.duration=Duration
           this.sourceType=SourceType
           this.paths=Paths
           /*paths is list of wastes object but stored in json string form*/
           /*user JSON.parse(waste.paths) toconvert into object and access values like normal array*/
           
           
       }
       return Waste;
   }());


   var Truck = /** @class */ (function () {
    
        function Truck(TruckPosLat,TruckPosLon,TruckId,TruckDriverName,TruckDriverPnumber,Truckwastes,Status,Distance,Duration,SelfRequest) {
            this.truckPosLat=TruckPosLat
            this.truckPosLon=TruckPosLon
            this.truckId=TruckId;
            this.truckDriverName=TruckDriverName;
            this.truckDriverPnumber=TruckDriverPnumber;
            this.truckwastes=Truckwastes;
            this.status=Status;
            this.distance=Distance;
            this.duration=Duration;
            this.selfRequest=SelfRequest
    
    
    
        }
        return Truck;
    }());

    var TruckLog = /** @class */ (function () {
        
           function TruckLog(TruckLogs) {
               this.truckLogs=TruckLog //string json of wastes and wastes has arrays of trucks
        
           }
           return TruckLog;
       }());


/*variable declarations dont make it global*/

let totalFunctionCall=0
let stackSetI=[]


exports.newNodeAdded=functions.firestore.document("wastes/{NEWDOC}").onCreate(event=>{
    let wasteContainer=[]
    let newWasteAdded    
    

    console.log("total function call "+totalFunctionCall++)
    return admin.firestore().collection("wastes").get().then((datasnap=>{
        let origin=[]
        let destinations=[]
        let recommendedWastes=[]
        let nearByWastes=[] //for whom it is concerned #i.e origin object

         newWasteAdded=event.data.data()
         origin.push(newWasteAdded.sourceLat+","+newWasteAdded.sourceLon)
        console.log("new document is "+newWasteAdded.sourceId)
        
        datasnap.forEach(each=>{
            console.log("wastes are "+each.data().sourceId)
            wasteContainer.push(each.data())
        })

        wasteContainer.forEach(each=>{
            if(each.sourceId==newWasteAdded.sourceId){}else{
                destinations.push(each.sourceLat+","+each.sourceLon)
                recommendedWastes.push(each)
                console.log(each.sourceId)
            }
        })


        new Promise((resolve,reject)=>{
        console.log("all the wastes added succesfully")
        distance_matrix.matrix(origin,destinations,function(err,response){
            console.log("json response "+JSON.stringify(response))
            if(response['status']=="OK"){
                var rowsObject=response['rows']
                rowsObject.forEach(eachROws=>{
                    var elements=eachROws['elements']
                    elements.forEach((eachElements,index)=>{ //two asrgs value and counter as eachelements and index
                        if(eachElements.status=="OK"){
        
    
                    //check if the order is same as java object and what if error comes
                    eachWaste=new Waste(
                        recommendedWastes[index].sourceLat,
                        recommendedWastes[index].sourceLon,
                        recommendedWastes[index].sourceId,
                        "N",
                        recommendedWastes[index].sourceType,
                        eachElements.distance.value,
                        eachElements.duration.value,
                        null
                        
                        )
                    nearByWastes.push(eachWaste)//hold the result for that object temporary
                    }
                       
                   })
                })

                /*sort the wastes*/
                console.log("sorting data started in "+new Date().getSeconds())
                nearByWastes.sort((waste1,waste2)=>{
                    return waste1.distance-waste2.distance
                })
                console.log("sorting completed in "+new Date().getSeconds())
        
                resolve()
           }else{
               console.log("error in the distance matrix calls")
            
               reject();
           }
        })

    }).then(()=>{
        let paths=new Object()
                nearByWastes.forEach((each,index)=>{
                    console.log("ascending order "+each.sourceId)
                    paths[index]=each
                })
                
                console.log("Update the newly added node with wastes nearby")
                console.log("doc id "+event.data.id)
                
                console.log("waste object "+JSON.stringify(newWasteAdded))
                newWasteAdded.paths=paths
                return event.data.ref.update({paths:JSON.stringify(newWasteAdded.paths)}).then((res)=>{


                    return admin.firestore().collection("wastes").doc(event.data.id).get().then((doc=>{
                        console.log(doc.data())

                        let paths=JSON.parse(doc.data().paths)
                        console.log("paths value "+paths[0].sourceId)
                    }))
                    
                })
                
        }).catch(()=>{
            console.log("error in the distance matrix ")
        })
        
    }))
})


/*Handle truck driver request */

exports.pickersRequest=functions.firestore.document("pickers/{PushId}").onUpdate(updateEvent=>{
    let newUpdateDoc=updateEvent.data.data()
    if(!newUpdateDoc.selfRequest){return }
    newUpdateDoc.selfRequest=false//to avoid infinte loop

    return updateEvent.data.ref.set(newUpdateDoc).then(res=>{
        console.log("Truck driver request came from id "+updateEvent.data.data().truckId)
        
        console.log("update events")
        return admin.firestore().collection("wastes").get().then(datasnap=>{
            console.log("getting wastes ")
            
            let wasteContainer=[] /*it has waste object except distance and duration value*/
            let nearByWastes=[] /*it contains waste object as well as distance and duration value in sorted order*/
            let origin=[]
            let destinations=[]

            datasnap.forEach(each=>{
                wasteContainer.push(each.data())
                destinations.push(each.data().sourceLat+","+each.data().sourceLon)
                console.log("each "+each.data())
            })
            origin.push(newUpdateDoc.truckPosLat+","+newUpdateDoc.truckPosLon)
            console.log("origin is  "+origin[0])
            

            new Promise((resolve,reject)=>{
                console.log("running distance matrix ")
                
                distance_matrix.matrix(origin,destinations,(error,response)=>{
                    console.log("json response "+JSON.stringify(response))
                    if(response['status']=="OK"){
                        var rowsObject=response['rows']
                        rowsObject.forEach(eachROws=>{
                            var elements=eachROws['elements']
                            elements.forEach((eachElements,index)=>{ 
                                if(eachElements.status=="OK"){
                                    let paths=[]
                                    let object=JSON.parse(wasteContainer[index].paths)                                    
                                    for(var i in object) {
                                        paths.push(object[i]);
                                    }
                
                               eachWaste=new Waste(
                                wasteContainer[index].sourceLat,
                                wasteContainer[index].sourceLon,
                                wasteContainer[index].sourceId,
                                "N",
                                wasteContainer[index].sourceType,
                                eachElements.distance.value,//this two distance and duration only needs to get updated else is all same 
                                eachElements.duration.value,
                                paths
                                )
                                nearByWastes.push(eachWaste)
                            }
                               
                           })
                        })
        
                        /*sort the wastes in ascending order for desc=>waste2.distance-waste1.distance*/
                        console.log("sorting data started in "+new Date().getSeconds())
                        nearByWastes.sort((waste1,waste2)=>{
                            return waste1.distance-waste2.distance
                        })
                        console.log("sorting completed in "+new Date().getSeconds())
              
                        nearByWastes[0].paths.forEach(each=>{
                            console.log("each "+each.sourceId)
                        })
                        
                        resolve()
                   }else{
                    console.log("error in the distance matrix calls")
                    reject()
                    
                   }
    
                })

            }).then(()=>{
                console.log("Chain algorithmI starts")

                return new Promise((resolve,reject)=>{
                    stackSetI.push(nearByWastes[0])
                    for(var i=0;i<3;i++){
                        console.log("value of i"+i)
                        for(var j=0;stackSetI[stackSetI.length-1].paths.length;j++){
                            console.log("top of stack is "+stackSetI[stackSetI.length-1].sourceId)
                            console.log("next possibility "+stackSetI[stackSetI.length-1].paths[j].sourceId)
                            
                            if(!presentInStack(stackSetI[stackSetI.length-1].paths[j])){
                                findAndAddWaste(nearByWastes,stackSetI[stackSetI.length-1].paths[j])
                                break
                            }else{
                                continue
                            }
    
                        }
                    }
                    console.log("only runs after loop because loop doesnot waits")
                    console.log("starting from "+nearByWastes[0].sourceId)
                    
                    stackSetI.forEach(each=>{
                        console.log("chain is "+each.sourceId)
                    })

                    resolve()

                }).then(()=>{
                    return updateEvent.data.ref.update({truckwastes:JSON.stringify(stackSetI)}).then(res=>{
                        console.log("updated finally")
                    })
        

                })
             
           }).catch(()=>{
            console.log("error in the loop when chain algorithm started ")
        })

        })

    })


})


/* function to handle waste cancelled events*/

exports.wasteCancelled=functions.firestore.document("wastes/{ANYDOC}").onUpdate(wasteSnapshot=>{
    let cancelledDoc=wasteSnapshot.data.data()
    
    if(cancelledDoc.sourceStatus=="unpicked" | cancelledDoc.sourceStatus=="picked"){
        return
    }
    let truckNearby=[]
    let totalTrucks=[]
    let destinations=[]
    let origin=[]
    let wasteInLogs=[]
    let wastePositionInLogs
    let trucksInWaste=[]//previous trucks in that waste
    
    let cancelledDocExistInLog=false



    return admin.firestore().collection("pickers").get().then(truckSnapshot=>{

        truckSnapshot.forEach(eachTruck=>{
            destinations.push(eachTruck.data().truckPosLat+","+eachTruck.data().truckPosLon)
            totalTrucks.push(eachTruck)
        })
        origin.push(cancelledDoc.sourceLat+","+cancelledDoc.sourceLon)
        new Promise((resolve,reject)=>{
            console.log("running distance matrix ")
            
            distance_matrix.matrix(origin,destinations,(error,response)=>{
                console.log("json response "+JSON.stringify(response))
                if(response['status']=="OK"){
                    var rowsObject=response['rows']
                    rowsObject.forEach(eachROws=>{
                        var elements=eachROws['elements']
                        elements.forEach((eachElements,index)=>{ 
                            if(eachElements.status=="OK"){
                            
                          let eachTruck=new Truck(
                            totalTrucks[index].truckPosLat,
                            totalTrucks[index].truckPosLon,
                            totalTrucks[index].truckId,
                            totalTrucks[index].truckDriverName,
                            totalTrucks[index].truckDriverPnumber,
                            null,
                            totalTrucks[index].status,
                            eachElements.distance.value,
                            eachElements.duration.value,
                            totalTrucks[index].selfRequest
                            
                            )
                            truckNearby.push(eachTruck)
                        }
                           
                       })
                    })
                    resolve()
               }else{
                console.log("error in the distance matrix calls")
                reject()
                
               }
    
            })
    
        }).then(()=>{
            console.log("before distance matrix it is not called")
    
            /*sort the trucks nearby ascending*/
    
            truckNearby.sort((truck1,truck2)=>{
                return truck1.distance-truck2.distance
            })
            console.log("sorting completed ")
            
            return admin.firestore().doc("recommendedTrucks/logs").get().then(logs=>{
                console.log("josn log "+JSON.stringify(logs.val()))
                
            let logDoc=JSON.parse(log.data.data()) //arrays of wastes
            
            if(logDoc==null | logDoc.trucklogs=="" | logDoc.truckLogs==null){
                console.log("no data on logs")
                console.log("adding wastes with nearby truck for the first time")
                let truckLogs=[]
                let newWaste=cancelledDoc
                let trucks=[]
                if(truckNearby[0]!=null){
                    trucks.push(truckNearby[0])
                }
                newWaste.trucks=trucks
                truckLogs.push(newWaste)
    
    
                /*add the logs to database*/
                return log.ref.set(trucklogs).then(response=>{
                    console.log("new waste added to the logs")
    
                }).catch(()=>{
                    console.log("error because inner arrays cannot be ")
                })
    
                
            }else{
            
            /*chain with another then */
            return new Promise((resolve,reject)=>{
            console.log("log is not empty")
    
            console.log("log doc data "+JSON.stringify(logDoc))
            // logDoc.forEach(eachWaste=>{
            //     //add wastes
            //     wasteInLogs.push(eachWaste)
            // })
    
            // //check waste if exist
            // wasteInLogs.forEach((each,index)=>{
            //     if(each.sourceId==cancelledDoc.sourceId){
            //         cancelledDocExistInLog=true
            //         wastePositionInLogs=index
            //         //break
            //     }
            // })
    
            // if(cancelledDocExistInLog){
            //     wasteInLogs[wastePositionInLogs].trucks.forEach(eachTruck=>{
            //         trucksInWaste.push(eachTruck)
            //     })
            // }
    
    
        })
    }


        })

    })

    
})


})
function presentInStack(object){
    for(var i=0;i<stackSetI.length;i++){
        if(object.sourceId==stackSetI[i].sourceId){
            console.log("present")
            return true
        }
    }
    console.log("not present")
    
    return false;
}

function findAndAddWaste(nearByWastes,object){
    let nextNode
    for(var i=0;i<nearByWastes.length;i++){
        if(object.sourceId==nearByWastes[i].sourceId){
            nextNode= nearByWastes[i]
            console.log("next node "+nextNode.sourceId)            
            break
        }
    }
    stackSetI.push(nextNode)

}

/*cloud function for freecall app*/

exports.freecallDatabase=functions.database.ref("users/9813054341/friends/{anydoc}/{messages}/").onUpdate(datasnapShot=>{
    console.log("new datasnapshot "+JSON.stringify(datasnapShot.data.val()))
    console.log("old datasnapshot "+JSON.stringify(datasnapShot.data.previous.val()))

})















